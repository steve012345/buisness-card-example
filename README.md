## Table Of Contents:
* [General Information](#general-info)
    * [Name](#name)
    * [Phone Number](#phone)
    * [Email Address](#email)
* [Packages](#packages)
    * [Java](#java)
    * [Python](#python)
* [Setup](#setup)
* [Unit Tests](#unittests)

## General Information
The objective of this project is to create a sample Business Card reader that 
can read the business card data as a string, and extract the name, phone number, 
and email of the individual on that business card. If successful, the application
will print out the results to the user.

This application was developed in an IDE called PyCharm, which is developed by
JetBrains (I highly recommend it). You don't have to download PyCharm to use this
application, but just in case you are interested in installing this, simply 
download it below:

    https://www.jetbrains.com/pycharm/

## Name
The project utilizes a Machine Learning library that is integrated with ntlk 
that is used to read and extract english-written human names from a text. This 
requires the use of some Java Libraries however. Fortunately, I have provided 
those libraries in this package; all that will need to be provided is either the 
Java executable path or the Java Executable Path (this can be set in your PATH 
variable). 

This will only extract the first name on the Business Card rather than the list 
of valid names (since there were no requirements to extract all of the names, so 
the application will return the first one. This can be changed in a future release).

## Phone Number
The project extracts the Phone Number by using a Regex string to filter and parse
the number out. As well, it will look for certain keywords to extract the phone 
number from strings as: 

    "Tel", "Telephone", "Phone", "Cell", "Cell Phone", "Work", "Work Phone", 
    "Office", "Office Phone"
    
The number formats that this application accepts includes:

    ###-###-####
    (###)###-####
    +# (###) ###-####
    
    where # is a digit from 0-9

This will extract the first number on the list, so if you have multiple different
numbers from the list, you will get only the first one pulled out of the string 
(there was no requirement to extract multiple phone numbers from a single business 
card, so I extracted one rather than multiple different numbers at once. However, 
that can be changed in a future release).
    
## Email Address
The project extracts the Email Address by using a simple regex to filter and extract
the email address from the string. Now, if there are multiple different email entries,
it will return the first email from that string (since there was no requirement to
extract multiple email addresses. However, if the requirements do change, this can be
amended in a future release).

## Packages
This project requires the use of both Java and Python 3. A breakdown of the different 
languages and packages are shown below:

## Java

The java version used is Java JDK 8. To install Java 8 on your GNU/Linux Box, 
(depending on your distro, and depending on the repository that you are using),
simply use:

    Debian/Ubuntu:
    sudo apt-get update
    sudo apt install openjdk-8-jdk openjdk-8-jre

    Fedora/CentOS (for CentOS 7 and below):
    sudo yum update
    sudo yum install java-1.8.0-openjdk
    
    CentOS 8:
    sudo dnf update
    sudo dnf install java-1.8.0-openjdk-devel

    For the rest of the distros:
    Update your packages to the latest and greatest
    Install Java 8.
    
The directory stanford_ner contains all of the tools and packages required for 
the Business Card app to read the card and extract the human names, so the 
user does not have to extract and pull anything extra. 

Now, to set your Java Path, simply use:

    <choice of text editor> ~/.bashrc
    export JAVA_HOME=<Path to your Java Executable>
    source ~/.bashrc
    

## Python
Used Python 3.8.5 (current one as of 08/06/2020), along with:

* pip version: 20.2.1
* setuptools version: 49.3.0
* regex version: 2020.7.14
* ntlk version: 3.5
* numpy version: 1.19.1
* nameparser version: 1.0.6
* tqdm version: 4.48.2
* joblib version: 0.16.0
* click version: 7.1.2

To install Python on your GNU/Linux system (depending on your distro, and 
depending on the repository that you are using), use:

Debian/Ubuntu:

    sudo apt-get update
    sudo apt-get install python3 

Fedora/CentOS (for CentOS 7 and below):

    sudo yum update
    sudo yum install python3 
    
CentOS 8:

    sudo dnf update
    sudo dnf install python3 

For the rest of the distros:

    Update your packages to the latest and greatest
    Install python.
    
Next, you would want to update Python with the above packages with pip. If 
you want to install pip (depending on your distro, and depending on the 
repository that you are using), simply do:

    Debian/Ubuntu:
    sudo apt-get update
    sudo apt-get install python3-pip 

    Fedora/CentOS (for CentOS 7 and below):
    sudo yum update
    sudo yum install python3-pip
    
    CentOS 8:
    sudo dnf update
    sudo dnf install python3-pip

    For the rest of the distros:
    Update your packages to the latest and greatest
    Install python-pip.

Then, you can either create your own python virtual environment for the 
project:

    Create your personal venv:   
    python3 -m venv <path to your own venv>   
    
    Set up your venv to run this package:
    cd <path to your own venv>
    source <path to your own venv>/bin/activate
    
    # Install the listed packages above:
    pip install <package>
    
    # To check if it is installed:
    pip list
    
## Setup
After setting up the Virtual Environment, you can run the application:

    python <path to project>/BuisnessCard.py --input <your string> --java <path to Java EXE>
    
Where:

    --input, -i : The input string to the appilcation
    --java, -j  : The Java EXE for the name extraction. This can be 
                  picked up automatically if you set your JAVA_HOME 
                  environment variables, so this is optional.

You should see the results of the application printed to you on your terminal:

    python <path to project>/BuisnessCard.py --input " ASYMMETRIK LTD Mike Smith Senior Software Engineer (410)555-1234 msmith@asymmetrik.com"
    
    [nltk_data] Downloading package wordnet to
    [nltk_data]     C:\Users\Stephen\AppData\Roaming\nltk_data...
    [nltk_data]   Package wordnet is already up-to-date!
    [+] The name is: Mike Smith
    [+] The phone number is: (410)555-1234
    [+] The email address is: msmith@asymmetrik.com
    
##Unit Tests
Next is dealing with the Unit Tests of this application. The tests are stored in 
the directory UnitTests. Unfortunately, for theses tests, there is no feature to 
add the java executable path to extract from. One workaround is to add the 
environment variable as an input to execute the tests. Simply perform:

    JAVAHOME=<Path To Java EXE>/java python -m unittest discover <test_directory>

This should run all of the tests outlined within the Unit Tests directory. If all
of these tests passed, that should give the user confidence that the application 
is working appropriately. There are a total of 13 tests that this package will 
check for (testing the get_phone_number(), get_email(), and get_name() methods).
The results should look something like this (for now, the deprecated warning will
stay until another update has been made):

    [-] Require ASCII/Unicode String to parse phone numbers. Cannot take Bytes-like string:  cannot use a string pattern on a bytes-like object
    [-] Require ASCII/Unicode String to parse email. Cannot take Bytes-like string:  cannot use a string pattern on a bytes-like object
    <Path To venv>\lib\site-packages\nltk\tag\stanford.py:199: DeprecationWarning: 
    The StanfordTokenizer will be deprecated in version 3.2.6.
    Please use nltk.parse.corenlp.CoreNLPParser instead.
      super(StanfordNERTagger, self).__init__(*args, **kwargs)
    [-] Require ASCII/Unicode String to parse English like names. Cannot take Bytes-like string:  cannot use a string pattern on a bytes-like object
    [-] Requires ASCII/Unicode String to perform operations on the string:  '_io.BytesIO' object has no attribute 'split'
    [-] Require ASCII/Unicode String to parse email. Cannot take Bytes-like string:  expected string or bytes-like object
    [-] Require ASCII/Unicode String to parse English like names. Cannot take Bytes-like string:  expected string or bytes-like object
    [-] Requires ASCII/Unicode String to perform operations on the string:  '_io.StringIO' object has no attribute 'split'
    [-] Require ASCII/Unicode String to parse email. Cannot take Bytes-like string:  expected string or bytes-like object
    [-] Require ASCII/Unicode String to parse English like names. Cannot take Bytes-like string:  expected string or bytes-like object