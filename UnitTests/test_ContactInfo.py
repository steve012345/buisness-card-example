"""
File that is used to run basic unit tests.
"""

####################
# Imported Modules #
####################
import unittest
import random
import string
import os
import io

from ContactInfo import ContactInfo as CI

##############################
# Helper Functions for Tests #
##############################


def random_string_module(str_size: int) -> str:
    """
    Function to allow one to generate a random series of strings, broken up into
    phrases. It may almost look like crazy ciphertext, but this allows us to validate
    whether or not it can pick up the desired string.
    :param str_size: The size of the random string.
    :return: The random string.
    """
    return ''.join(random.choice(string.ascii_letters) for _ in range(str_size))


def create_random_test_string(test_str: str, str_len: int) -> str:
    """
    Function that will generate a test string to exfil out the test string for our test.
    :param test_str: The content which we want to see whether our functions can exfil out
                     this string or not.
    :param str_len: The length of the randomly generated test string.
    :return: The randomly generated test string. If str_len is less than the length of test_str, return test_str
    """

    if str_len < len(test_str):
        return test_str

    length_left = str_len - len(test_str)
    result = ""

    # Generate the random parts of the garbled sentence that we want to test with.
    while length_left > 0:
        chunk_size = random.randint(0, length_left)
        result += random_string_module(chunk_size) + ' '
        length_left -= (chunk_size + 1)

        if length_left < 0:
            result.strip(" ")

    # Pick a random space within the string, and insert our data into the string.
    # Return the resulting string for testing.
    split_result = result.split(" ")
    test_str_indx = random.randint(0, len(split_result))
    output = ' '.join(split_result[:test_str_indx]) + " " + test_str + " " + ' '.join(split_result[test_str_indx:])
    return output.strip(" ")


def create_random_english_sentence(test_str: str) -> str:
    """
    Function that will be used to help create a random uncoherent test sentence.
    :param test_str: The test string to enter into our basic english sentence.
    :return: The basic crummy English sentence.
    """

    # Selecting parts of our sentence.
    nouns = ("puppy", "car", "rabbit", "girl", "monkey")
    verbs = ("runs", "hits", "jumps", "drives", "sleeps")
    adv = ("crazily.", "dutifully.", "foolishly.", "merrily.", "occasionally.")
    adj = ("adorable", "clueless", "dirty", "odd", "stupid")

    # Construct our random english sentence.
    sentence_choice = [nouns, verbs, adj, adv]
    rand_sentence = [random.choice(x) for x in sentence_choice]
    index = random.randint(0, 3)
    rand_sentence = rand_sentence[:index] + [test_str] + rand_sentence[index:]

    return ' '.join(rand_sentence)


def shuffle_string(word: str) -> str:
    """
    Helper function to shuffle word/letters around to make an input unrecognizable.
    :param word: The string which to shuffle
    :return: The shuffled string.
    """
    word = list(word)
    random.shuffle(word)
    return ''.join(word)

############################
# Test Classes and Objects #
############################


class MyTestCase(unittest.TestCase):

    # Default that is stored for the java binary. However, it may not be useful here.
    JAVAHOME = os.path.abspath(os.path.join(os.sep,
                                            'usr',
                                            'bin',
                                            'java'))

    default_cards = [

        """    
        ASYMMETRIK LTD
        Mike Smith
        Senior Software Engineer
        (410)555-1234
        msmith@asymmetrik.com
        """,

        """    
        ASYMMETRIK LTD\n
        Mike Smith\n
        Senior Software Engineer\n
        (410)555-1234\n
        msmith@asymmetrik.com\n
        """,

        """
        Foobar Technologies
        Analytic Developer
        Lisa Haung
        1234 Sentry Road
        Columbia, MD 12345
        Phone: 410-555-1234
        Fax: 410-555-4321
        lisa.haung@foobartech.com
        """,

        """
        Foobar Technologies\r\n
        Analytic Developer\r\n
        Lisa Haung\r\n
        1234 Sentry Road\r\n
        Columbia, MD 12345\r\n
        Phone: 410-555-1234\r\n
        Fax: 410-555-4321\r\n
        lisa.haung@foobartech.com\r\n
        """,

        """
        Arthur Wilson
        Software Engineer
        Decision & Security Technologies
        ABC Technologies
        123 North 11th Street
        Suite 229
        Arlington, VA 22209
        Tel: +1 (703) 555-1259
        Fax: +1 (703) 555-1200
        awilson@abctech.com
        """,

        """
        Arthur Wilson\n
        Software Engineer\n
        Decision & Security Technologies\n
        ABC Technologies\n
        123 North 11th Street\n
        Suite 229\n
        Arlington, VA 22209\n
        Tel: +1 (703) 555-1259\n
        Fax: +1 (703) 555-1200\n
        awilson@abctech.com\n
        """,

    ]

    random_names_list = [
        "Laticia Villalba",
        "Shara Rister",
        "Nestor Galligan",
        "Michael Jordan",
        "Dani Junge",
        "Kathlyn Nero",
        "Marta Gongora",
        "Sanjuanita Beaird",
        "Gertrude Donlan",
        "Guillermina Haney",
        "Erick Northcott",
        "Annie Mcelwain",
        "Leroy Mehaffey",
        "Ileen Cadwell",
        "Toya Olmos",
        "Kary Koo",
        "Tonja Cheeks",
        "Herma Hufnagel",
        "Melina Delgado",
        "Toi Hux"
    ]

    random_email_list = [
        "malin@msn.com",
        "janneh@aol.com",
        "wetter@me.com",
        "ilyaz@att.net",
        "msroth@sbcglobal.net",
        "satishr@yahoo.com",
        "tmccarth@yahoo.com",
        "avalon@att.net",
        "muadip@yahoo.ca",
        "library@msn.com",
        "kingma@icloud.com",
        "purvis@live.com",
        "ovprit@optonline.net",
        "pkplex@optonline.net",
        "jguyer@live.com",
        "moonlapse@outlook.com",
        "feamster@icloud.com",
        "petersen@outlook.com",
        "bescoto@yahoo.ca",
        "rfisher@yahoo.ca"
    ]

    random_number_list = [
        "958-317-3417",
        "Tel: +1 (255) 754-3687",
        "Work: +1 (405) 597-2731",
        "396-586-5392",
        "Tel: +1 (700) 485-3991",
        " 812-966-9807 ",
        "Telephone: 998-789-7938",
        "Office Phone: (222)651-5446",
        "Office: (372)507-2509",
        " (604)263-6539 ",
        "  Tel: +1 (627) 804-2637",
        "   445-542-0098",
        "  (318)398-6532  ",
        " +1 (333) 754-8976"
        "Cell: +1 (994) 848-0437",
        " Tel: +1 (257) 911-8912",
        "   743-945-0672",
        "Phone: +1 (849) 369-0516  ",
        "Cell Phone: (634)259-8369",
        " Work Phone: +1 (682) 445-5248  ",
        "  861-747-4192"
    ]

    default_cards_results = [
        {
            "name": "Mike Smith",
            "number": "(410)555-1234",
            "email": "msmith@asymmetrik.com"
        },
        {
            "name": "Mike Smith",
            "number": "(410)555-1234",
            "email": "msmith@asymmetrik.com"
        },
        {
            "name": "Lisa Haung",
            "number": "410-555-1234",
            "email": "lisa.haung@foobartech.com"
        },
        {
            "name": "Lisa Haung",
            "number": "410-555-1234",
            "email": "lisa.haung@foobartech.com"
        },
        {
            "name": "Arthur Wilson",
            "number": "+1 (703) 555-1259",
            "email": "awilson@abctech.com"
        },
        {
            "name": "Arthur Wilson",
            "number": "+1 (703) 555-1259",
            "email": "awilson@abctech.com"
        }
    ]

    bad_number_formats = [
        "+1-958-317-3417",
        "Tel: +1-(255)-754-3687",
        "Work: 1 (405) 597-2731",
        "3965865392",
        "Tel:+1-700-485-3991",
        "+999-812-966-9807 ",
        "Telephone: -1-998-789-7938",
        "Office Phone: 222 651-5446",
        "Office 372 507 2509",
        " 604+263-6539 ",
        "  Tel: +1 +2 (627) 804-2637",
        "   445-5420098",
        "  (+1 318)398-6532  ",
        " +1 ((333) 754-8976)"
        "Cell: (+1 (994) 848-0437)",
        " Tel: (+1-257-911-8912)",
        "   (743-945-0672)",
        "PhoneCallz: -1 (849) 369-0516  ",
        "Cell Phone: ((634)259-8369)",
        " Work Phone: +1 682 445 5248  ",
        "  8617474192 +1"
    ]

    non_english_names = [
        "στέφανος",
        "יעקב",
        "リューク",
        "आभा",
        "Александр"
    ]

    def test_contact_info(self):
        """
        Function that tests whether or not our object can extract the given input.
        :return: N/A
        """

        for buisness_card_data, buisness_card_results in zip(self.default_cards, self.default_cards_results):

            cinfo = CI.ContactInfo(buisness_card_data)

            self.assertEqual(cinfo.get_name(), buisness_card_results["name"])
            self.assertEqual(cinfo.get_email(), buisness_card_results["email"])
            self.assertEqual(cinfo.get_phone_number(), buisness_card_results["number"])

    def test_phone_numbers(self):
        """
        Function that tests whether or not we can extract the phone numbers from a string.
        :return: N/A
        """

        # Tagged words that will indicate a number of importance.
        buzz_words = ("Tel", "Telephone", "Phone", "Cell", "Cell Phone", "Work", "Work Phone", "Office", "Office Phone")

        for number in self.random_number_list:

            # Random string.
            test_string = create_random_test_string(number, random.randint(len(number), 100))

            cinfo = CI.ContactInfo(text=test_string)

            # Slow, but in testing, we only care about how simple it is for one to
            # follow and understand what is going on. Performance is not as big of
            # an issue.
            if any(x in number for x in buzz_words):
                for word in buzz_words:
                    if word in number and number.split(f"{word}: ")[0].strip() == '':
                        self.assertEqual(cinfo.get_phone_number(), number.split(f"{word}: ")[1].strip())
            else:
                self.assertEqual(cinfo.get_phone_number(), number.strip())

            # English string
            english_string = create_random_english_sentence(number)

            einfo = CI.ContactInfo(text=english_string)

            # Slow, but in testing, we only care about how simple it is for one to
            # follow and understand what is going on. Performance is not as big of
            # an issue.
            if any(x in number for x in buzz_words):
                for word in buzz_words:
                    if word in number and number.split(f"{word}: ")[0].strip() == '':
                        self.assertEqual(cinfo.get_phone_number(), number.split(f"{word}: ")[1].strip())
            else:
                self.assertEqual(einfo.get_phone_number(), number.strip())

    def test_names(self):
        """
        Function that tests whether or not we can extract the names from a string.
        This needs an english sentence, since a random sentence will not work correctly here,
        since the machine learning algorithm requires the english language to sample and pick
        out the name. It will not work in another language other than english.
        :return: N/A
        """

        os.environ['JAVAHOME'] = self.JAVAHOME

        for name in self.random_names_list:

            # English string.
            test_string = create_random_english_sentence(name)

            cinfo = CI.ContactInfo(text=test_string)

            self.assertEqual(cinfo.get_name(), name)

    def test_email(self):
        """
        Function that tests whether or not we can extract the email addresses from a string.
        :return: N/A
        """

        for email in self.random_email_list:

            # Random string.
            test_string = create_random_test_string(email, random.randint(len(email), 100))

            cinfo = CI.ContactInfo(text=test_string)

            self.assertEqual(cinfo.get_email(), email)

            # English string.
            english_string = create_random_english_sentence(email)

            einfo = CI.ContactInfo(text=english_string)

            self.assertEqual(einfo.get_email(), email)

    def test_failure_no_names(self):
        """
        Function that should fail if there is no name to be found in the string.
        Again, requires an english sentence for the machine learning algorithm to work.
        :return: N/A
        """

        os.environ['JAVAHOME'] = self.JAVAHOME

        for _ in self.random_names_list:

            # English string.
            test_string = create_random_english_sentence("")

            cinfo = CI.ContactInfo(text=test_string)

            self.assertEqual(cinfo.get_name(), None)

    def test_failure_no_numbers(self):
        """
        Function that should fail if there is no numbers to be found in the string.
        :return: N/A
        """

        for number in self.random_number_list:

            # Random string.
            test_string = create_random_test_string("", random.randint(0, 100))

            cinfo = CI.ContactInfo(text=test_string)

            self.assertNotEqual(cinfo.get_phone_number(), number)
            self.assertEqual(cinfo.get_phone_number(), None)

            # English string.
            english_string = create_random_english_sentence("")

            einfo = CI.ContactInfo(text=english_string)

            self.assertNotEqual(cinfo.get_phone_number(), number)
            self.assertEqual(einfo.get_phone_number(), None)

    def test_failure_no_email(self):
        """
        Function that should fail if there is no numbers to be found in the string.
        :return: N/A
        """

        for email in self.random_number_list:

            # Random string.
            test_string = create_random_test_string("", random.randint(0, 100))

            cinfo = CI.ContactInfo(text=test_string)

            self.assertNotEqual(cinfo.get_email(), email)
            self.assertEqual(cinfo.get_email(), None)

            # English string.
            english_string = create_random_english_sentence("")

            einfo = CI.ContactInfo(text=english_string)

            self.assertNotEqual(einfo.get_email(), email)
            self.assertEqual(einfo.get_email(), None)

    def test_failure_bad_email(self):
        """
        Function that tests whether or not we have a bad email address that makes no sense.
        :return: N/A
        """

        for email_address in self.random_email_list:

            # This should screw up the email address enough such that its unrecognizable.
            bad_email = random_string_module(5) + email_address.replace('@', random.choice(string.ascii_letters)) \
                        + random_string_module(5)

            # Random string.
            test_string = create_random_test_string(bad_email, random.randint(len(bad_email), 100))

            cinfo = CI.ContactInfo(text=test_string)

            self.assertNotEqual(cinfo.get_email(), email_address)
            self.assertEqual(cinfo.get_email(), None)
            
            # English string.
            english_string = create_random_english_sentence(bad_email)

            einfo = CI.ContactInfo(text=english_string)

            self.assertNotEqual(einfo.get_email(), email_address)
            self.assertEqual(einfo.get_email(), None)

    def test_failure_bad_name(self):
        """
        Function to test whether or not our Machine Learning Algorithm will fail if
        it is given a terribly bad name that is not English by any means (or at least,
        a recognizable English Name).
        :return: N/A
        """

        os.environ['JAVAHOME'] = self.JAVAHOME

        for name in self.random_names_list:

            # Create our bad name to use for the test...
            bad_name = random_string_module(5) + name.split(" ")[0].strip() + random_string_module(5) + " " + \
                       random_string_module(5) + name.split(" ")[1].strip() + random_string_module(5)

            # English String.
            english_string = create_random_english_sentence(bad_name)

            einfo = CI.ContactInfo(text=english_string)

            self.assertNotEqual(einfo.get_name(), name)
            self.assertEqual(einfo.get_name(), None)

    def test_failure_bad_number(self):
        """
        Function to test whether or not the regex will pick up on a bad cellular number.
        :return: N/A
        """

        # Loop to check if we can scramble the numbers to which the function cannot extract the information.
        for number in self.random_number_list:

            # Create our own bad number string for us to use...
            bad_number = number.rstrip() + str(random.randint(0, int(str(0xffffffff))))

            # Random string.
            bad_string = create_random_test_string(bad_number, random.randint(len(bad_number), 100))

            cinfo = CI.ContactInfo(text=bad_string)

            self.assertNotEqual(cinfo.get_phone_number(), number)
            self.assertEqual(cinfo.get_phone_number(), None)

            # English string.
            english_string = create_random_english_sentence(bad_string)

            einfo = CI.ContactInfo(text=english_string)

            self.assertNotEqual(einfo.get_phone_number(), number)
            self.assertEqual(einfo.get_phone_number(), None)

        # Give reasonably bad input, and see if it fails appropriately.
        for number in self.bad_number_formats:

            # Create our own bad number string for us to use...
            bad_number = number.rstrip() + str(random.randint(0, int(str(0xffffffff))))

            # Check straight up number.
            cinfo = CI.ContactInfo(text=number)
            self.assertNotEqual(cinfo.get_phone_number(), number)
            self.assertEqual(cinfo.get_phone_number(), None)

            # Check random string.
            bad_string = create_random_test_string(bad_number, random.randint(len(bad_number), 100))

            rinfo = CI.ContactInfo(text=bad_string)

            self.assertNotEqual(rinfo.get_phone_number(), number)
            self.assertEqual(rinfo.get_phone_number(), None)

            # English string.
            english_string = create_random_english_sentence(bad_string)

            einfo = CI.ContactInfo(text=english_string)

            self.assertNotEqual(einfo.get_phone_number(), number)
            self.assertEqual(einfo.get_phone_number(), None)

    def test_bad_bytes_and_bytes_like_string(self):
        """
        Function that will test whether or not the function will fail if bad string types
        (ASCII, Unicode, Bytes, BytesIO/StringIO like string) are used as inputs. Python 3, by default, uses
        Unicode as the default. Tests use ASCII and Unicode.
        :return: N/A
        """

        phone_type_except = "Require ASCII/Unicode String to parse phone numbers. Cannot take Bytes-like string."
        email_type_except = "Require ASCII/Unicode String to parse email. Cannot take Bytes-like string."
        name_type_except = "Require ASCII/Unicode String to parse English like names. Cannot take Bytes-like string"
        attr_except = "Requires ASCII/Unicode String to perform operations on the string"

        bytes_string = b"""    
        ASYMMETRIK LTD
        Mike Smith
        Senior Software Engineer
        (410)555-1234
        msmith@asymmetrik.com
        """

        bytesio_string = io.BytesIO(b"""    
        ASYMMETRIK LTD
        Mike Smith
        Senior Software Engineer
        (410)555-1234
        msmith@asymmetrik.com
        """)

        stringio_string = io.StringIO(
            """    
            ASYMMETRIK LTD
            Mike Smith
            Senior Software Engineer
            (410)555-1234
            msmith@asymmetrik.com
            """
        )

        """ Bytes String """
        binfo = CI.ContactInfo(text=bytes_string)

        # Test bytes like string with phone number
        with self.assertRaises(TypeError) as te:
            binfo.get_phone_number()
        self.assertTrue(phone_type_except in str(te.exception))

        # test bytes like string with email
        with self.assertRaises(TypeError) as te:
            binfo.get_email()
        self.assertTrue(email_type_except in str(te.exception))

        # test bytes like string with name
        with self.assertRaises(TypeError) as te:
            binfo.get_name()
        self.assertTrue(name_type_except in str(te.exception))

        """ Bytes IO String """
        bio = CI.ContactInfo(text=bytesio_string)

        # Test Bytes IO like string with phone number
        with self.assertRaises(AttributeError) as ae:
            bio.get_phone_number()
        self.assertTrue(attr_except in str(ae.exception))

        # test Bytes IO like string with email
        with self.assertRaises(TypeError) as te:
            bio.get_email()
        self.assertTrue(email_type_except in str(te.exception))

        # test Bytes IO like string with name
        with self.assertRaises(TypeError) as te:
            bio.get_name()
        self.assertTrue(name_type_except in str(te.exception))

        """ String IO String """
        sio = CI.ContactInfo(text=stringio_string)

        # Test String IO like string with phone number
        with self.assertRaises(AttributeError) as ae:
            sio.get_phone_number()
        self.assertTrue(attr_except in str(ae.exception))

        # test String IO like string with email
        with self.assertRaises(TypeError) as ae:
            sio.get_email()
        self.assertTrue(email_type_except in str(ae.exception))

        # test String IO like string with name
        with self.assertRaises(TypeError) as te:
            bio.get_name()
        self.assertTrue(name_type_except in str(te.exception))

    def test_fail_non_english_name(self):
        """
        Function that tests the get_name() function to tell whether or not it will fail if given
        non-english names (making the name written in English a requirement).
        :return: N/A
        """

        for non_english_name in self.non_english_names:

            einfo = CI.ContactInfo(text=non_english_name)

            self.assertNotEqual(einfo.get_name(), non_english_name)

            self.assertEqual(einfo.get_name(), None)

    def test_multiple_entries_for_names_numbers_and_emails(self):
        """
        Function that tests whether or not the functions will return the first entry that it finds if given
        multiple different valid entries in the same string.
        :return: N/A
        """

        test_str = """ 
        ASYMMETRIK LTD
        Mike Smith
        Senior Software Engineer
        (410)555-1234
        msmith@asymmetrik.com
        Arthur Wilson
        Software Engineer
        Decision & Security Technologies
        ABC Technologies
        123 North 11th Street
        Suite 229
        Arlington, VA 22209
        Tel: +1 (703) 555-1259
        Fax: +1 (703) 555-1200
        awilson@abctech.com
        """

        ci = CI.ContactInfo(text=test_str)

        # Test the name to see if we extract the first name found.
        self.assertEqual(ci.get_name(), "Mike Smith")
        self.assertNotEqual(ci.get_name(), "Arthur Wilson")

        # Test the phone number to see if we extract the first phone number.
        self.assertEqual(ci.get_phone_number(), "(410)555-1234")
        self.assertNotEqual(ci.get_phone_number(), "+1 (703) 555-1259")
        self.assertNotEqual(ci.get_phone_number(), "+1 (703) 555-1200")

        # Test the email address to make sure that we grab the first email address.
        self.assertEqual(ci.get_email(), "msmith@asymmetrik.com")
        self.assertNotEqual(ci.get_email(), "awilson@abctech.com")

############################
# Driver for Main Program. #
############################


if __name__ == '__main__':

    msg = """
          [+] Using provided Environment Variable that is in the ${PATH} environment Variable.\n
          [+] You can add this by entering the following command:\n
          [+] JAVAHOME=<Path To Java File>/java.exe python -m unittest discover <test_directory>\n
          [+] I know, it shouldn't be used at all like this, but without the Java Environment\n
          [+] variable, it would not extract the English names. 
          """

    print(msg)
    MyTestCase.JAVAHOME = os.environ.get('JAVAHOME', MyTestCase.JAVAHOME)

    unittest.main()
