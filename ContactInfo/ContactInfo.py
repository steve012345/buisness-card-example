"""
This file is used for the Asymmetrik Test on the Buisness Card OCR.
"""

####################
# Imported modules #
####################
import re
import os
import nltk

# Special modules, mainly for name recognition
from nltk.tag.stanford import StanfordNERTagger as Tagger

# Package wordnet that needs to be downloaded for ntlk for
# algorithm to operate efficiently.
nltk.download("wordnet")


##########################
# Functions and Methods. #
##########################


def _check_standard_number(number: str) -> bool:
    """
    Function that checks the standard number. Will check for the number format:

    ###-###-####

    :param number: The number to check for that number format.
    :return: Boolean that returns true if its exactly that format, false otherwise.
    """

    number_characters = ('(', ')')

    number_list = number.split("-")

    try:

        return len(number_list[0]) == 3 and len(number_list[1]) == 3 and len(number_list[2]) == 4 and \
               not any(x in number_list for x in number_characters)

    except IndexError:

        return False


def _check_parenthesis_number(number: str) -> bool:
    """
    Function that checks the number with a parenthesis. Will check the format like:

    (###)###-####

    :param number: The number to check for the proper format.
    :return: Boolean that returns true if its exactly that format, false otherwise.
    """

    number_characters = ('(', ')')

    number_list = number.split("-")

    try:

        return len(number_list[0]) == 8 and len(number_list[1]) == 4 \
               and any(x in number_list[0] for x in number_characters)

    except IndexError:

        return False


def _check_country_code_number(number: str) -> bool:
    """
    Function that checks the number with a country code. Will check for the format like:

    +# (###) ###-####

    :param number: The number to check for the proper format.
    :return: Boolean that returns true if its exactly that format, false otherwise.
    """

    number_characters = ('(', ')', '+')

    number_list = number.split(" ")[:-1] + number.split(" ")[-1].split("-")

    try:

        return len(number_list[0]) == 2 and number_characters[-1] in number_list[0] and len(number_list[1]) == 5 and \
               any(x in number_list[1] for x in number_characters[:-1]) and len(number_list[2]) == 3 and \
               len(number_list[3]) == 4

    except IndexError:

        return False


def extract_names(text: str) -> list:
    """
    Function to extract names in a line of text.
    :param text: The text to extract any names from.
    :return: The list of names which have been extracted.
    """

    try:

        # Local to the package. This is included in the package, so
        # you don't have to download it :)
        jarfile = os.path.join(os.path.realpath(os.path.dirname(__file__)), "..", 'stanford_ner', 'stanford-ner.jar')
        gzfile = os.path.join(os.path.realpath(os.path.dirname(__file__)), "..", 'stanford_ner',
                              'english.all.3class.distsim.crf.ser.gz')

        st = Tagger(gzfile, jarfile)

        found_numbers = list()

        # Lets give this a try; usually, this works very well (but sometimes, it misses some names...)
        for sent in nltk.sent_tokenize(text):
            tokens = nltk.tokenize.word_tokenize(sent)
            tags = st.tag(tokens)
            for index in range(len(tags) - 1):
                if tags[index][1] == 'PERSON' and tags[index + 1][1] == 'PERSON':
                    found_numbers.append(tags[index][0] + " " + tags[index + 1][0])

        # Check if we obtain any good found_numbers; if not, we will have to try something else.
        if len(found_numbers) > 0:
            return found_numbers

        person, person_list, name = list(), list(), ""

        # Must not have worked; try something else...
        text_tree = nltk.ne_chunk(nltk.pos_tag(nltk.tokenize.word_tokenize(text)), binary=False)
        for subtree in text_tree.subtrees(filter=lambda t: t.label() == 'PERSON'):
            person.append(subtree[0][0])

        # Clean up the appended string.
        for index in range(0, len(person), 2):
            person_list.append(' '.join(person[index:index + 2]))

        return person_list

    except TypeError as te:
        print("[-] Require ASCII/Unicode String to parse English like names. Cannot take Bytes-like string: ", te)
        raise TypeError("Require ASCII/Unicode String to parse English like names. Cannot take Bytes-like string")


def extract_phone_numbers(text: str) -> list:
    """
    Function that will extract the phone number(s) of the person.
    :param text: The string
    :return: A list of phone numbers that are discovered from the business card.
    :except: TypeError Exception.
    """

    try:

        # Regex for finding a number in a string. Not entirely great, but it does the job.
        r = re.compile(r'(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})')

        # Tagged words that will indicate a number of importance.
        buzz_words = ("Tel", "Telephone", "Phone", "Cell", "Cell Phone", "Work", "Work Phone", "Office", "Office Phone")

        tokenized_string = text.split()

        # Used to see what we initially got from a plain regex search.
        regex_numbers = r.findall(text)

        found_numbers = list()

        # Check the tokenized string for the keywords.
        for token in tokenized_string:

            # Does it have an appropriate label that we can check against?
            if any(x.lower() in token.lower() for x in buzz_words):
                index = tokenized_string.index(token)

                # Look at the components of the split string to find the actual number.
                if "+" in tokenized_string[index + 1] and '(' in tokenized_string[index + 2] and \
                        _check_country_code_number(' '.join(tokenized_string[index + 1: index + 4])):
                    found_numbers.append(' '.join(tokenized_string[index + 1: index + 4]).strip())

            # If not, lets use the default regex to grab the number that we are looking for.
            elif r.match(token) and _check_standard_number(token) or \
                    _check_parenthesis_number(token) or _check_country_code_number(token):
                found_numbers.append(token.strip())

        # Section which compares between the two to find the real numbers.
        results = list()
        for regex in regex_numbers:
            for number in found_numbers:
                if regex in number:
                    results.append(number.strip())

        return results

    except TypeError as te:
        print("[-] Require ASCII/Unicode String to parse phone numbers. Cannot take Bytes-like string: ", te)
        raise TypeError("Require ASCII/Unicode String to parse phone numbers. Cannot take Bytes-like string.")

    except AttributeError as ae:
        print("[-] Requires ASCII/Unicode String to perform operations on the string: ", ae)
        raise AttributeError("Requires ASCII/Unicode String to perform operations on the string")


def extract_email(email: str) -> list:
    """
    Function that will check the string for a proper email address
    :param email: The string to check if it is a proper email address
    :return: The email string; otherwise, None.
    :except: TypeError Exception.
    """

    try:

        # We don't have to do anything too crazy here. A simple regex will do.
        return re.findall(r"[\w\.-]+@[\w\.-]+", email)

    except TypeError as te:
        print("[-] Require ASCII/Unicode String to parse email. Cannot take Bytes-like string: ", te)
        raise TypeError("Require ASCII/Unicode String to parse email. Cannot take Bytes-like string.")


#######################
# Classes and Objects #
#######################


class ContactInfo(object):
    """
    Class to obtain the basic Contact Information from the Buisness Card.
    """

    def __init__(self, text=""):
        """
        Init function that can be used to initialize the class.
        :param text: The internal variable that can be passed to the other three methods.
        """
        self.text = text

    def get_name(self) -> None:
        """
        Function that retrieves the name from the text that was loaded.
        :return: The first name that is in the list as a string.
        """

        try:
            return extract_names(self.text)[0]
        except IndexError:
            return None

    def get_phone_number(self) -> None:
        """
        Function that allows you to extract the first contact phone number.
        :return: The first contact phone number in the list.
        """

        try:
            return extract_phone_numbers(self.text)[0]
        except IndexError:
            return None

    def get_email(self) -> None:
        """
        Function that will return the first email found.
        :return: The first email in the list.
        """

        try:
            return extract_email(self.text)[0]
        except IndexError:
            return None
