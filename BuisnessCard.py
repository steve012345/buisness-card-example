"""
Main file for the application.
"""

####################
# Imported Modules #
####################
from ContactInfo import ContactInfo as CI
import os
import argparse

###############################
# Driver to the main program. #
###############################


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-j", "--java", type=str,
                        dest="java", help="The Java Executable used for the ntlk library. Must be an absolute path.")
    parser.add_argument("-i", "--input", type=str,
                        required=True, dest="input", help="The string text input.")
    args = parser.parse_args()

    # If not provided, assume that the Java Environment Variable is already set.
    if os.path.exists(args.java) and os.path.isfile(args.java):
        print("[+] Provided appropriate Java File.")
        os.environ['JAVAHOME'] = args.java

    if not args.input:
        print("[-] ERROR: No input specified. You need to add an input string.")
        parser.print_usage()

    else:
        myinfo = CI.ContactInfo(args.input)

        # Print out our results of our class.
        print(f"[+] The name is: {myinfo.get_name()}")
        print(f"[+] The phone number is: {myinfo.get_phone_number()}")
        print(f"[+] The email address is: {myinfo.get_email()}")
